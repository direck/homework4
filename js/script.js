class Table{
    constructor (counter){
        let table = document.createElement('table'), tr, td, row, cell;
        for (row = 0; row < counter; row++) {
            tr = document.createElement('tr');
            for (cell = 0; cell < counter; cell++) {
                td = document.createElement('td');
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        document.body.appendChild(table);
    }
    clickListener () {
        document.body.addEventListener('click', event => {
            let eventTarget = event.target;       
            if (eventTarget.tagName === 'TD')
            eventTarget.classList.toggle('fill');
        });
    }
}

//create table
let table = new Table(30);

//click listener and change color
table.clickListener();

